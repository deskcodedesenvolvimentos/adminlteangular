import { FormEditorsComponent } from './pages/form-editors/form-editors.component';
import { FixedComponent } from './fixed/fixed.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { GeneralComponent } from './general/general.component';
import { IconsComponent } from './icons/icons.component';
import { SlidersComponent } from './sliders/sliders.component';
import { ModalsComponent } from './modals/modals.component';
import { CalendarComponent } from './calendar/calendar.component';
import { WidgetsComponent } from './widgets/widgets.component';
import { TimelineComponent } from './timeline/timeline.component';
import { BoxedComponent } from './boxed/boxed.component';
import { ColapsedSidebarComponent } from './colapsed-sidebar/colapsed-sidebar.component';
import { ChartJsComponent } from './chart-js/chart-js.component';
import { MorrisComponent } from './morris/morris.component';
import { FlotComponent } from './flot/flot.component';
import { InlineChartsComponent } from './inline-charts/inline-charts.component';
import { FormGeneralComponent } from './pages/form-general/form-general.component';
import { FormAdvancedComponent } from './pages/form-advanced/form-advanced.component';


export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'dashboard2', component: Dashboard2Component },

    { path: 'top-nav', component: TopNavComponent },
    { path: 'boxed', component: BoxedComponent },
    { path: 'fixed', component: FixedComponent },
    { path: 'collapsed-sidebar', component: ColapsedSidebarComponent },

    { path: 'widgets', component: WidgetsComponent },

    { path: 'chartjs', component: ChartJsComponent },
    { path: 'morris', component: MorrisComponent },
    { path: 'flot', component: FlotComponent },
    { path: 'inline-charts', component: InlineChartsComponent },

    { path: 'calendar', component: CalendarComponent },

    { path: 'buttons', component: ButtonsComponent },
    { path: 'general', component: GeneralComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'timeline', component: TimelineComponent },
    { path: 'sliders', component: SlidersComponent },
    { path: 'modals', component: ModalsComponent },

    { path: 'formrs/general', component: FormGeneralComponent },
    { path: 'formrs/advanced', component: FormAdvancedComponent },
    { path: 'formrs/editors', component: FormEditorsComponent },





];