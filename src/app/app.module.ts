import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { GeneralComponent } from './general/general.component';
import { IconsComponent } from './icons/icons.component';
import { SlidersComponent } from './sliders/sliders.component';
import { TimelineComponent } from './timeline/timeline.component';
import { ModalsComponent } from './modals/modals.component';
import { WidgetsComponent } from './widgets/widgets.component';
import { CalendarComponent } from './calendar/calendar.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { BoxedComponent } from './boxed/boxed.component';
import { FixedComponent } from './fixed/fixed.component';
import { ColapsedSidebarComponent } from './colapsed-sidebar/colapsed-sidebar.component';
import { ChartJsComponent } from './chart-js/chart-js.component';
import { MorrisComponent } from './morris/morris.component';
import { FlotComponent } from './flot/flot.component';
import { InlineChartsComponent } from './inline-charts/inline-charts.component';
import { FormGeneralComponent } from './pages/form-general/form-general.component';
import { AdvancedComponent } from './pages/forms/advanced/advanced.component';
import { EditorsComponent } from './pages/forms/editors/editors.component';
import { FormAdvancedComponent } from './pages/form-advanced/form-advanced.component';
import { FormEditorsComponent } from './pages/form-editors/form-editors.component';


@NgModule({
  declarations: [
    AppComponent,
    ButtonsComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    Dashboard2Component,
    GeneralComponent,
    IconsComponent,
    SlidersComponent,
    TimelineComponent,
    ModalsComponent,
    WidgetsComponent,
    CalendarComponent,
    TopNavComponent,
    BoxedComponent,
    FixedComponent,
    ColapsedSidebarComponent,
    ChartJsComponent,
    MorrisComponent,
    FlotComponent,
    InlineChartsComponent,
    FormGeneralComponent,
    AdvancedComponent,
    EditorsComponent,
    FormAdvancedComponent,
    FormEditorsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
